package ito.poo.Excepciones;

public class ExcepcionValidaFechaRegreso extends Exception{

	private static final long serialVersionUID = 1L;

	public  ExcepcionValidaFechaRegreso(String msg) {
		super(msg);
	}
}
